#!/bin/bash
#TODO: fix MAX_LIKE_PER_TAG -- make this number smaller (because of increased nubmer of searched tags)
#TODO: doubletap should tap in ViewProfile position (sometimes stuck)

# exit on any errors
# set -e

install() {
  log "Copy doubletap"
  ${ADB} push ./files/doubletap-${1} /sdcard/doubletap
  log "Copy done"
}
# Utils
log() {
  echo "`date '+%Y-%m-%d %H:%M'` -- $1"
}
slp() {
  S=${1:-0.5}
  log "sleep for $S..."
  sleep $S
}
rand() {
  echo $(shuf -n 1 -i $1)
}
rand_dig() {
  DELIM=${3:-"."}
  echo $(shuf -n 1 -i $1)${DELIM}$(shuf -n 1 -i $2)
}
# Go To
go_to() {
  X=$1
  Y=$2
  swX=$(expr $X - 6)
  swY=$(expr $Y - 6)
  HOLD=$(rand 100-200)
  log "$FUNCNAME - $a_SWIPE $X $Y $swX $swY $HOLD"
  $a_SWIPE $X $Y $swX $swY $HOLD
  echo ""
}

go_home() {
  log "$FUNCNAME"
  $a_SHELL input keyevent KEYCODE_HOME
}

go_back() {
  log "$FUNCNAME"
  $a_SHELL input keyevent KEYCODE_BACK
}
go_profile() {
  log "$FUNCNAME"
  go_to $PROFILE
}
go_search() {
  log "$FUNCNAME"
  go_to $SEARCH
}
go_more() {
  log "$FUNCNAME"
  go_to $MORE  # Press more on tags list (need to kill insta every time)
}
swipe() {
  log "$FUNCNAME"
  #     start X   start Y   end X   end Y   swipe time
  $a_SWIPE $(rand 700-900) $(rand 1300-1500) $(rand 700-900) $(rand 300-400) $(rand 190-270)
  slp 0.$(rand "2-9")
  close_keyboard
}

search_tag() {
  # TODO: clean text
  # TODO: if InputShown else ???
  TAGS_COUNT=$((${#TAGS[@]}-1))
  TAGS_RAND=$(rand "0-${TAGS_COUNT}")
  TAG="${TAGS[${TAGS_RAND}]}"
  log "$FUNCNAME tag [${TAGS_RAND}] $TAG "
  go_profile
  slp $(rand "2-5")
  go_search
  slp $(rand "2-5")
  go_to $SEARCH_BAR
  slp $(rand "2-5")
  InputShown=$(${ADB} shell dumpsys input_method | grep mInputShown=true)
  if [ "${InputShown}" ]; then
    ${ADB} shell "input text '${TAG}'"
    slp $(rand "2-5")
    go_to ${SEARCH_TAG}
    slp $(rand "2-5")
    go_to ${SEARCH_FIRST_TAG}
    slp $(rand "5-7")
    go_to ${RECENT}
  fi
}

# Exec
close_keyboard() {
  InputShown=$(${ADB} shell dumpsys input_method | grep mInputShown=true)
  if [ "${InputShown}" ]; then
    log "$FUNCNAME - Keyboard is open -- closing it"
    slp $(rand "2-5")
    go_back
    slp $(rand "2-5")
    go_back
    slp $(rand "2-5")
  fi
}
kill_instagram() {
  ${ADB} shell am force-stop com.instagram.android || log "not running?"
  log "$FUNCNAME - killed"
}
run_instagram() {
  log "$FUNCNAME"
  # Set lowest brightness
  ${ADB} shell settings put system screen_brightness_mode 0
  ${ADB} shell settings put system screen_brightness 0
  # Force landscape
        ${ADB} shell settings put system accelerometer_rotation 0
  ${ADB} shell content insert --uri content://settings/system --bind name:s:user_rotation --bind value:i:1

  swipe
  swipe
  log "killing running instagram"
  kill_instagram
  go_home
  slp 1
  go_home
  # ${ADB} shell am start -n com.instagram.android || log "not running?"
  ${ADB} shell monkey -p com.instagram.android  -c android.intent.category.LAUNCHER 1
}

run_main() {
  # vars
  max_likes=$( rand ${MAX_LIKE_PER_TAG}-$(($MAX_LIKE_PER_TAG+10)) )
  half_likes=$(($max_likes/2))
  current_likes=0
  tag_no=1

  # Take a random tag from profile bio
  search_tag
  slp $(rand 3-5)

  # Need to skip videos at the beggining
  $a_SWIPE $(rand 700-900) $(rand 1300-1500) $(rand 700-900) $(rand 300-400) $(rand 190-270)
  slp $(rand 3-5)
  
  while [ ${current_likes} -lt ${max_likes} ]; do
    swipe_n_like

    #       cur_like + like_or_not(1/0)
    current_likes=$(($current_likes+$?))
    log "like: $current_likes - max: $max_likes"
    

    # # sometimes go back 
    # if [ ${current_likes} -gt ${half_likes} ]; then
    #   log "${current_likes} is bigger than half: ${half_likes}"
    #   if [ $(rand $randGoBack) -eq 1 ]; then
    #     go_back
    #   fi

    # fast scroll
    if [ ${current_likes} -lt 2 ]; then
      if [ $(rand $randFastScroll) -eq 1 ]; then
        scroll=0
        scroll_times=$(rand $randFastScrollTimes)
        while [ ${scroll} -lt ${scroll_times} ]; do
          log "fast scroll: $scroll of $scroll_times"
          swipe
          scroll=$(($scroll+1))
        done
      fi
    fi


  done

  # afterparty
  log "done with TAG: $tag_no ($tag)"
  current_likes=0
  slp
  kill_instagram
  slp $(rand $randSleepAfterKill)
  run_instagram
  slp $(rand $randSleepAfterStart)
}

doubletap() {
  $a_SHELL "cat /sdcard/doubletap > ${DOUBLETAP_INPUT} && sleep 0.12 && cat /sdcard/doubletap > ${DOUBLETAP_INPUT}"
}

check_activity() {
  # mCurrentFocus=Window{35bfeaf5 u0 com.instagram.android/com.instagram.mainactivity.MainActivity}
  local RUNNING=$( ${ADB} shell dumpsys window windows | grep -E 'mCurrentFocus' | grep -c instagram )
  if [ "${RUNNING}" == "0" ]; then
    log "was not running insta, lauching it"
    $go_back
    slp $(rand_dig "1-3" "1-10")
    $go_back
    ${ADB} shell monkey -p com.instagram.android  -c android.intent.category.LAUNCHER 1
  fi
}

swipe_n_like() {
  # Swiping
  while [ $(rand $randSwipe) -eq 2 ]; do
    swipe
    # check if we still in instagram app
  done

  check_activity

  # Doubletap
  if [ $(rand $randLike) -eq 2 ]; then
    doubletap
    # to be able to like from profile to photo direcly
    slp $(rand_dig "1-3" "1-10")
    doubletap
    slp $(rand_dig $randSleepAfterLike "10-90")
    log "liked!"
    slp $(rand_dig $randSleepAfterLike "10-90")
    swipe
    return 1
  else
    log "not liked"
    return 0
  fi
}

# TODO: fix arg check
if [ -z "${1}" ]; then
  log "no first arg"
  exit 1
elif [ -z "${2}" ]; then
  log "no second arg"
  exit 1
fi


# TODO: fix path
source /home/pi/dNull/ues.insta/config.sh
# TODO: better configs (not source then, but parse)
source /home/pi/dNull/ues.insta/${2}.sh
source /home/pi/dNull/ues.insta/tags.sh

ADB="adb -s ${ADB_id}"
a_SHELL="${ADB} shell"
a_TAP="${ADB} shell input tap"
a_EVENT="${ADB} shell sendevent /dev/input/event2"
a_SWIPE="${ADB} shell input touchscreen swipe"

log "Instagram ADB auto-liker by shellshock"
case ${1} in 
  install)
    install ${2}
    ;;
  run)
    run_instagram
    slp $(rand 10-15)
    while true; do
      run_main
    done
    ;;
  stop|exit)
    kill_instagram
    ;;
  fail)
    exit 1
    ;;
  comment)
    write_comment
    exit 0
    ;;
  debug)
    check_activity
    ;;
  *)
    log "dont know what to do"
    exit 0
    ;;
esac



# TIPS
# https://superuser.com/questions/1173378/using-adb-to-sendevent-touches-to-the-phone-but-cant-release
# adb shell getevent | grep --line-buffered ^/ | tee /tmp/android-touch-events.log
# awk '{printf "%s %d %d %d\n", substr($1, 1, length($1) -1), strtonum("0x"$2), strtonum("0x"$3), strtonum("0x"$4)}' /tmp/android-touch-events.log | xargs -l adb shell sendevent

# some noname site
# adb shell
# cd /sdcard/
# cat /dev/input/event1 > doubletap
# cat doubletap > /dev/input/event1 && sleep 0.1 && cat doubletap > /dev/input/event1
